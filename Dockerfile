FROM python:3.7

WORKDIR /usr/src/app

COPY Pipfile Pipfile.lock ./

RUN \
  pip install --upgrade pip && \
  pip install pipenv && \
  pipenv install --dev --system --deploy

COPY . ./

ENV PYTHONDEVMODE=1 PYTHONUNBUFFERED=1

EXPOSE 8080
