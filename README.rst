===============================
Case study - financial data API
===============================

Requirements
============
* `Docker <https://docs.docker.com/install/>`_ and `docker-compose <https://docs.docker.com/compose/install/>`_

Getting started
===============
Clone the repository and start the server:

.. code-block:: shell

    git clone https://gitlab.com/lzinsou/financial-api.git
    cd financial-api
    docker-compose up app

Open the link in a browser to access GraphiQL: http://localhost:8080/graphql

Using any other GraphQL client is also possible.

As no auth mechanism is implemented, a user can simulate being logged in by adding a ``Company-Id`` and a ``User-Id`` header to the request.

GraphiQL is modified to do this automatically if passed the ``company`` and ``user`` URL parameters.
For example: http://localhost:8080/graphql?company=myCompany&user=me

Hint: to access master wallets, the company has to be the same as the owner defined in ``config/default.ini`` (i.e.: Spendesk).

To stop the server, just use *Ctrl + C*.

Build a new image after configuration or code changes:

.. code-block:: shell

    docker-compose build app

Stop and remove all containers, images, and volumes by running:

.. code-block:: shell

    docker-compose down --rmi local --volume

Notes
=====
This project only has minimal dependencies:

- AIOHTTP (web client and server)
- graphql-core-next (GraphQL library)
- Motor (MongoDB client)

Since this API provides finanical data, some entities (wallets and cards) are ledger-based: the single source of truth regarding their balance is their history of fund transfers.
This removes the need for complex transactions to avoid discrepancies in case of write failure. However, this means the balance needs to be computed from transfers, and may cause performance issues.
See the caveats.


Caveats
=======
- GraphQL schema traversal capabilities are underexploited here. It would make sense to be able to query a list of cards on the wallet object, among other possibilities.
- To keep the GraphQL schema as simple as possible, Relay is not implemented (no standard base 64 encoded IDs, no common Node lookup for refetch, no standard for mutation input and payload, no cursor-paginated connection types).
- Entity IDs are MongoDB ObjectIds, cast into strings for easier manipulation within GraphQL resolvers. Ideally, they should instead be handled as a GraphQL custom scalar, or hidden in base 64-encoded IDs, Relay-style.
- Ledgers (cards and wallets) always recompute their balance property, from their history of fund transfers. For performance reasons, this balance property should be cached.
- The SDL schema could be modularised, but for now it's simple enough so keeping it in a single file is readable.
- Card numbers are handled as strings because of graphql's 32 bit int limitation.
- The ugly mix of snake case (Python object methods and properties) and camel case (GraphQL) could be avoided by introducing some magic, but wasn't a hard requirement here.
- Database accesses are not optimised, there will be n+1 issues on some queries. To fix this, dataloaders are to be introduced to cache and batch loading of entities.
- The database is not properly indexed at this point. Indexes would be needed on transfer origin IDs and target IDs, for example.
- AIOHTTP's "no globals" approach implies using GraphQL resolve info to access app resources (like the database). Resolve info will be passed around *a lot*. Using Entity managers and dataloaders could help, but not much.
- Error types used in resolvers should be more specific.
- Processing transactions may need to gain robustness (using a queue, and probably a retry mechanism).
- A simple MongoDB storage is good for fast prototyping, but the ledger solution may require some specific write-only storage, and a cached read mechanism (a full on event-driven CQRS architecture?).
- There are a lot of possible asyncio and AIOHTTP optimisations (the most obvious one being a faster even loop), but this would just add noise to the project at this stage.
- Only the GraphQL over HTTP behaviour is tested. The business logic isn't.
- Code quality tools still report issues.
