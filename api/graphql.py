"""GraphQL over HTTP.

Guidelines from .. _GraphQL docs: https://graphql.org/learn/serving-over-http/
"""
import json
from dataclasses import dataclass, field
from enum import Enum
from typing import TYPE_CHECKING, Any, Awaitable, Callable, Dict, Optional

import multidict
from aiohttp import web
from graphql import ExecutionResult, GraphQLSchema, graphql

from . import errors

# pylint: disable=invalid-name
if TYPE_CHECKING:
    Query = multidict.MultiDictProxy[str]  # pragma: no cover
else:
    Query = multidict.MultiDictProxy

# pylint: enable=invalid-name
QueryArgs = Dict[str, Any]
BodyArgs = Dict[str, Any]


# Routing
METHOD_POST = "POST"
DEFAULT_GRAPHQL_PATH = "/graphql"

# keys from HTTP request query and JSON body, used as GraphQL arguments
QUERY = "query"
VARIABLES = "variables"
OPERATION_NAME = "operationName"

# GraphQL over HTTP JSON response keys
DATA = "data"
ERRORS = "errors"


class ContentType(str, Enum):
    """Supported Content-Type HTTP request headers.

    :Example:

    Content-Type: application/json
    """

    GRAPHQL = "application/graphql"
    JSON = "application/json"


@dataclass(frozen=True)
class GraphQLArgs:
    """GraphQL arguments."""

    query: str
    variables: Dict[str, Any] = field(default_factory=dict)
    operation_name: str = ""


async def parse_body(
    body_json: Callable[[], Awaitable[BodyArgs]],
    body_text: Callable[[], Awaitable[str]],
    content_type: str,
) -> BodyArgs:
    """Parse the body of the HTTP request into a dict.

    Behaviour described in .. the
    _GraphQL docs: https://graphql.org/learn/serving-over-http/#post-request

    :param body_json: coroutine that returns the HTTP body as JSON data
    :param body_text: coroutine that returns the HTTP body as plain text
    :raise: web.HTTPUnsupportedMediaType if Content-Type is neither
            'application/graphql' or 'application/json'
    :raise: errors.BadJSONBodyError if it fails to read the request's body as
            JSON
    :return: body data as a dict:
             if HTTP request Content-Type is 'application/graphql', the dict
             has a single 'query' key with the full request body text as value;
             if HTTP request Content-Type is 'application/json', the dict is
             the request body parsed as JSON.
    """
    if content_type == ContentType.GRAPHQL:
        return {QUERY: await body_text()}

    if content_type == ContentType.JSON:
        try:
            body_args = await body_json()
            return body_args if isinstance(body_args, dict) else {}
        except json.JSONDecodeError as error:
            raise errors.BadJSONBodyError from error

    raise errors.UnsupportedContentType(supported=ContentType)


def parse_query_variables(raw_variables: str) -> Dict[str, Any]:
    """Parse a string value (i.e. from the HTTP query) into GraphQL variables.

    :param string: the raw variables string
    :raise: errors.BadJSONVariablesError if it fails to load the query's
            'variables' string field as JSON
    :return: a dict of GraphQL variables
    """
    try:
        variables = json.loads(raw_variables)
        return variables if isinstance(variables, dict) else {}
    except json.JSONDecodeError as error:
        raise errors.BadJSONVariablesError from error


def parse_query(query: Query) -> QueryArgs:
    """Parse the query from an HTTP request into a dict of arguments.

    .. note::
        If the 'variables' key is present in the dict, its value is parsed as
        JSON into a dict.

    :param query: the query from an HTTP request
    :return: a dict of arguments
    """
    query_args: QueryArgs = {**query}

    raw_query_variables = query_args.get(VARIABLES)
    if raw_query_variables is not None:
        query_args[VARIABLES] = parse_query_variables(raw_query_variables)

    return query_args


def merge_args(
    query_args: QueryArgs, body_args: Optional[BodyArgs] = None
) -> GraphQLArgs:
    """Get GraphQL args by merging query and body args from an HTTP request.

    .. note::
        Query arguments take precedence over body arguments.

    :param query_args: dict of arguments from the query of an HTTP request
    :param body_args: dict of arguments from the body of an HTTP request
    :return: GraphQL arguments object
    """
    body_args = body_args or {}
    args = {**body_args, **query_args}

    if args.get(QUERY) is None:
        raise errors.MissingQueryError

    return GraphQLArgs(
        query=args[QUERY],
        variables=args.get(VARIABLES) or {},
        operation_name=args.get(OPERATION_NAME) or "",
    )


def graphql_response(result: ExecutionResult) -> web.Response:
    """Render a GraphQL execution result as an HTTP JSON response.

    Behaviour described in .. the
    _GraphQL docs: https://graphql.org/learn/serving-over-http/#response

    :param result: GraphQL execution result
    :return: HTTP JSON response
    """
    data: Dict[str, Any] = {DATA: result.data or {}}

    if result.errors:
        data[ERRORS] = [error.formatted for error in result.errors]

    return web.json_response(data)


def configure_handler(
    schema: GraphQLSchema, root_value: Any = None
) -> Callable[[web.Request], Awaitable[web.Response]]:
    """Configure a GraphQL HTTP handler.

    :param request: GraphQL schema
    :return: HTTP request handler
    """
    if not isinstance(schema, GraphQLSchema):
        raise ValueError("Argument is not a GraphQL schema.")

    async def handler(request: web.Request) -> web.Response:
        query_args = parse_query(request.query)

        body_args: BodyArgs = {}
        if request.method == METHOD_POST and request.can_read_body:
            body_args = await parse_body(
                body_json=request.json,
                body_text=request.text,
                content_type=request.content_type,
            )

        args = merge_args(query_args, body_args)

        try:
            result = await graphql(
                schema,
                args.query,
                variable_values=args.variables,
                operation_name=args.operation_name,
                root_value=root_value,
                context_value=request,
            )
        except TypeError as error:
            raise errors.ArgumentTypeError from error

        return graphql_response(result)

    return handler


def register_handler(
    app: web.Application,
    schema: GraphQLSchema,
    path: str = DEFAULT_GRAPHQL_PATH,
    root_value: Any = None,
) -> None:
    """Configure a GraphQL over HTTP handler class and register its routes."""
    handler = configure_handler(schema, root_value=root_value)
    app.add_routes([web.get(path, handler), web.post(path, handler)])
