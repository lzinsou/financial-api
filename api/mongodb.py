"""MongoDB resource for an AIOHTTP web app."""
from configparser import SectionProxy
from typing import AsyncIterator

from aiohttp import web
from motor import motor_asyncio
from pymongo import IndexModel, ASCENDING, DESCENDING

MONGODB = "mongodb"


def register_mongodb(app: web.Application, config: SectionProxy) -> None:
    """Configure a MongoDB resource and register it to the web app."""

    async def mongodb(app: web.Application) -> AsyncIterator[None]:
        client = motor_asyncio.AsyncIOMotorClient(
            config["host"], config.get("port")
        )
        db = client[config["database"]]
        transfer_origin_unique_index = IndexModel([
                ("originId", ASCENDING),
                ("originSequenceNumber", ASCENDING)
            ],
            unique=True
        )
        transfer_target_unique_index = IndexModel([
                ("targetId", ASCENDING),
                ("targetSequenceNumber", ASCENDING)
            ],
            unique=True
        )
        db.transfers.create_indexes(
            [transfer_origin_unique_index, transfer_target_unique_index]
        )
        app[MONGODB] = db
        yield
        client.close()

    app.cleanup_ctx.append(mongodb)
