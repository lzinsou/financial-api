"""HTTP error formatting middleware."""
from typing import Awaitable, Callable, Dict, Iterable

from aiohttp import web
from aiohttp.web import middleware

ERROR = "error"


# pylint: disable=too-many-ancestors
class MissingQueryError(web.HTTPBadRequest):
    """Raised when there's no GraphQL query string in the HTTP request."""

    reason = "Must provide query string."


class BadJSONBodyError(web.HTTPBadRequest):
    """Raised when it fails to parse JSON in POST request body."""

    reason = "Bad JSON in POST request body."


class BadJSONVariablesError(web.HTTPBadRequest):
    """Raised when it fails to parse 'variables' JSON in HTTP request query."""

    reason = "Bad JSON in 'variables' query parameter."


class ArgumentTypeError(web.HTTPBadRequest):
    """Raised when an argument parsed from an HTTP request is of a bad type."""

    reason = "Argument type error."


class UnsupportedContentType(web.HTTPUnsupportedMediaType):
    """Raised when it does not support the Content-Type for an HTTP request."""

    def __init__(self, supported: Iterable[str]):
        """Set the reason as a hint about supported content types.

        :param supported: Supported content types
        """
        super().__init__(
            reason=(
                "Content-Type header must be one of the following MIME types: "
                "{}.".format(", ".join(supported))
            )
        )


@middleware
async def json_errors(
    request: web.Request,
    handler: Callable[[web.Request], Awaitable[web.StreamResponse]],
) -> web.StreamResponse:
    """Format any HTTP error as a JSON response.

    Try to return the handler's response, return the HTTP Error formatted as as
    JSON response if the handler raises one.

    :param request: HTTP request
    :param handler: HTTP request handler
    :return: HTTP response
    """
    try:
        return await handler(request)
    except web.HTTPError as error:
        message: str = error.reason
        if error.__cause__:
            message += " " + str(error.__cause__)

        data: Dict[str, str] = {ERROR: message}
        return web.json_response(data, status=error.status_code)
