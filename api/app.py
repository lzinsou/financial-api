"""Entry point of the app, where configuration and resources are set up."""
import configparser
import os
from aiohttp import web

from api.errors import json_errors
from api.fixtures.master_wallets import setup_master_wallets
from api.graphiql import graphiql
from api.graphql import register_handler
from api.mongodb import MONGODB, register_mongodb
from api.rates import register_rates
from api.schema import Root, SCHEMA

DEFAULT_CONFIG = "../config/default.ini"
API_CONFIG_KEY = "API"


def run() -> None:
    """Run the app."""
    config = configparser.ConfigParser()
    config.read(os.path.join(os.path.dirname(__file__), DEFAULT_CONFIG))

    middleware = []
    if config["graphiql"]["enable"] == "True":
        middleware.append(graphiql)
    middleware.append(json_errors)

    app = web.Application(middlewares=middleware)

    app["OWNER"] = config[API_CONFIG_KEY]["owner"]
    app["FEE"] = float(config[API_CONFIG_KEY]["fee"])

    register_rates(app)
    register_mongodb(app, config[MONGODB])
    register_handler(app, SCHEMA, root_value=Root())

    app.on_startup.append(setup_master_wallets)

    web.run_app(app, port=8080)
