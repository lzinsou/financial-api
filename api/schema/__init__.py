"""Schema module initialisation Provide a SCHEMA constant and a Root type."""
import os

from graphql import build_schema, GraphQLSchema

from api.schema.currency import register_currency_type_values
from api.schema.root import Root
from api.schema.currency import Currency

SDL = "schema.graphql"


def configure_schema(sdl: str) -> GraphQLSchema:
    """Build a GraphQL schema from an SDL file."""
    with open(os.path.join(os.path.dirname(__file__), sdl)) as schema_file:
        schema = build_schema(schema_file.read())

        # graphql-core-next doesn't add default values to enum types
        register_currency_type_values(schema)

    return schema


SCHEMA = configure_schema(SDL)

__all__ = ["Root", "SCHEMA"]
