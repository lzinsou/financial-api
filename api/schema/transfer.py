"""Transfer module."""
from dataclasses import dataclass, InitVar
from datetime import datetime
from typing import Optional

from graphql import GraphQLResolveInfo
from pymongo.errors import WriteError

from api.schema.currency import Currency
from api.schema.entity import Entity
from api.schema.ledger import Ledger
from api.schema.utils import (
    get_rates,
    get_user_id,
    get_master_wallet,
    get_company_id,
)


@dataclass
class Transfer(Entity):
    """A transfer of funds between ledgers."""

    collection = "transfers"

    timestamp: int
    amount: float
    originCurrency: Currency
    targetCurrency: Currency
    originId: str
    targetId: str
    originType: str
    targetType: str
    originSequenceNumber: int
    targetSequenceNumber: int
    userId: str
    rate: Optional[float] = None
    fee: Optional[float] = None
    _id: InitVar[Optional[str]] = None

    @classmethod
    async def create(
        cls,
        info: GraphQLResolveInfo,
        origin: Ledger,
        target: Ledger,
        amount: float,
    ):
        """Create a transfer between ledgers."""
        if await origin.balance(info) < amount:
            raise ValueError(
                "Insufficient funds, cannot proceed with the transfer."
            )

        transfer = cls(
            timestamp=int(datetime.now().timestamp()),
            amount=amount,
            originCurrency=origin.currency,
            targetCurrency=target.currency,
            originId=origin.id,
            targetId=target.id,
            originSequenceNumber=origin.sequenceNumber,
            targetSequenceNumber=target.sequenceNumber,
            originType=type(origin).__name__,
            targetType=type(target).__name__,
            userId=get_user_id(info),
        )

        if origin.currency != target.currency:
            transfer.rate = await get_rates(info).latest_rate(
                base=origin.currency, symbol=target.currency
            )

            if get_company_id(info) != info.context.app["OWNER"]:
                transfer.fee = amount * transfer.rate * info.context.app["FEE"]

        try:
            await transfer.persist(info)
        except WriteError:
            raise RuntimeError("Unable to create transfer: concurrency error!")
        await origin.bump_sequence(info)
        await target.bump_sequence(info)

        if transfer.fee:
            await cls.create(
                info,
                origin=target,
                target=get_master_wallet(info, target.currency),
                amount=transfer.fee,
            )

        return transfer
