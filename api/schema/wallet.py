"""Schema module for the Wallet type."""
from dataclasses import dataclass, InitVar
from typing import Optional

from graphql import GraphQLResolveInfo

from api.schema.currency import Currency
from api.schema.ledger import Ledger
from api.schema.utils import get_company_id


@dataclass
class Wallet(Ledger):
    """A wallet belonging to a company."""

    collection = "wallets"

    companyId: str
    isMaster: bool = False
    offset: float = 0.0
    _id: InitVar[Optional[str]] = None

    @classmethod
    async def create(
        cls, info: GraphQLResolveInfo, currency: Currency, amount: float = 0.0
    ):
        """Create a wallet."""
        # Use the ledger offset to start with some funds
        wallet = cls(
            companyId=get_company_id(info), currency=currency, offset=amount, sequenceNumber=0,
        )
        await wallet.persist(info)
        return wallet
