"""Schema module for the Currency type."""
from enum import Enum

from graphql import GraphQLSchema


class Currency(str, Enum):
    """Currency Enum type, with string values."""

    USD = "USD"
    GBP = "GBP"
    EUR = "EUR"


def register_currency_type_values(schema: GraphQLSchema) -> None:
    """Register the Currency type values with pre-existing schema type."""
    for name, value in schema.get_type("Currency").values.items():
        value.value = Currency[name].value
