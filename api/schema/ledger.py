"""Ledger module."""
from abc import ABC, abstractmethod
from dataclasses import dataclass

import bson
from graphql import GraphQLResolveInfo

from api.schema.entity import Entity
from api.schema.currency import Currency
from api.schema.utils import get_db


@dataclass
class Ledger(Entity, ABC):
    """A ledger is a "book" of transactions.

    It exposes a balance expressed in a currency.
    The balance is computed from the history of transaction for the ledger.
    """

    currency: Currency
    sequenceNumber: int

    # Offset allowing the creation of ledgers with funds coming from nowhere.
    # Generally considered a *dirty hack*!!!
    @property
    @abstractmethod
    def offset(self) -> float:
        """Concrete ledgers must implement an offset field."""
        raise NotImplementedError

    async def bump_sequence(self, info: GraphQLResolveInfo) -> int:
        """Bump sequence number."""
        result = await get_db(info)[self.collection].update_one(
            {"_id": bson.ObjectId(self.id)},
            {"$inc": {"sequenceNumber": 1}}
        )
        if result.modified_count > 0:
            self.sequenceNumber += 1
            return self.sequenceNumber

    async def balance(self, info: GraphQLResolveInfo) -> float:
        """Replay transactions to obtain the ledger's balance."""
        total = self.offset

        async for transfer_doc in get_db(info).transfers.find(
            {"$or": [{"originId": self.id}, {"targetId": self.id}]}
        ):
            amount = transfer_doc["amount"]
            if transfer_doc["originId"] == self.id:
                total = total - amount
            if transfer_doc["targetId"] == self.id:
                rate = transfer_doc["rate"]
                if rate:
                    total = total + amount * rate
                else:
                    total = total + amount

        return total
