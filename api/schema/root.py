"""Schema module for the Root type."""
from typing import List

from graphql import GraphQLResolveInfo

from api.schema.card import Card
from api.schema.currency import Currency
from api.schema.transfer import Transfer
from api.schema.utils import get_company_id, get_db, get_user_id
from api.schema.wallet import Wallet


class Root:
    """The root resolvers (schema root includes both Query and Mutation)."""

    @staticmethod
    async def wallets(info: GraphQLResolveInfo) -> List[Wallet]:
        """List wallets."""
        return [
            Wallet(**doc)
            async for doc in get_db(info).wallets.find(
                {"companyId": get_company_id(info)}
            )
        ]

    @staticmethod
    async def createWallet(
        info: GraphQLResolveInfo, currency: Currency, amount: float
    ) -> Wallet:
        """Create a wallet."""
        return await Wallet.create(info, currency, amount)

    @staticmethod
    async def cards(info: GraphQLResolveInfo) -> List[Card]:
        """List cards."""
        return [
            Card(**doc)
            async for doc in get_db(info).cards.find(
                {"userId": get_user_id(info)}
            )
        ]

    @staticmethod
    async def createCard(info: GraphQLResolveInfo, walletId: str) -> Card:
        """Create a card."""
        wallet = await Wallet.get(info, walletId)
        return await Card.create(info, wallet)

    @staticmethod
    async def blockCard(info: GraphQLResolveInfo, cardId: str) -> Card:
        """Block a card."""
        card = await Card.get(info, cardId)
        await card.block(info)
        return card

    @staticmethod
    async def unblockCard(info: GraphQLResolveInfo, cardId: str) -> Card:
        """Unblock a card."""
        card = await Card.get(info, cardId)
        await card.unblock(info)
        return card

    @staticmethod
    async def loadCard(
        info: GraphQLResolveInfo, cardId: str, amount: float
    ) -> Card:
        """Load funds on a card."""
        card = await Card.get(info, cardId)
        await card.load(info, amount)
        return card

    @staticmethod
    async def unloadCard(
        info: GraphQLResolveInfo, cardId: str, amount: float
    ) -> Card:
        """Unload funds from a card."""
        card = await Card.get(info, cardId)
        await card.unload(info, amount)
        return card

    @staticmethod
    async def transfer(
        info: GraphQLResolveInfo,
        originWalletId: str,
        targetWalletId: str,
        amount: float,
    ) -> Transfer:
        """Transfer funds."""
        origin_wallet = await Wallet.get(info, originWalletId)
        target_wallet = await Wallet.get(info, targetWalletId)

        if not (
            get_company_id(info)
            == origin_wallet.companyId
            == target_wallet.companyId
        ):
            raise ValueError(
                "You can only transfer funds between your own wallets."
            )

        if origin_wallet.id == target_wallet.id:
            raise ValueError("Cannot transfer funds to the same wallet.")

        return await Transfer.create(
            info, origin=origin_wallet, target=target_wallet, amount=amount
        )
