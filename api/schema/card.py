"""Schema module for the Card type."""
from __future__ import annotations

from dataclasses import dataclass, InitVar
from datetime import date
from random import randint
from typing import Optional

import bson
from graphql import GraphQLResolveInfo

from api.schema.ledger import Ledger
from api.schema.transfer import Transfer
from api.schema.utils import get_company_id, get_db, get_user_id
from api.schema.wallet import Wallet


@dataclass
class Card(Ledger):
    """A card belonging to a user and attached to a wallet."""

    collection = "cards"

    walletId: str
    number: str
    ccv: str
    expiry: str
    userId: str
    blocked: bool
    offset: float = 0.0
    _id: InitVar[Optional[str]] = None

    @classmethod
    async def create(cls, info: GraphQLResolveInfo, wallet: Wallet) -> Card:
        """Create a card attached to a wallet, with default values."""
        if wallet.companyId != get_company_id(info):
            raise ValueError(
                "You are not allowed attach a card to this wallet."
            )

        if wallet.id is None:
            raise ValueError("Cannot create card for a transient wallet.")

        today = date.today()
        card = cls(
            walletId=wallet.id,
            number=str(randint(1000000000000000, 9999999999999999)),
            ccv=str(randint(100, 999)),
            expiry="/".join([str(today.month + 1), str(today.year)]),
            blocked=False,
            currency=wallet.currency,
            userId=get_user_id(info),
            sequenceNumber=0,
        )
        await card.persist(info)
        return card

    async def load(self, info: GraphQLResolveInfo, amount: float) -> None:
        """Load funds on from the wallet the card is attached to."""
        if self.userId != get_user_id(info):
            raise ValueError("You are not allowed to load this card.")

        if self.blocked:
            raise ValueError("Cannot load a blocked card.")

        wallet = await Wallet.get(info, self.walletId)
        await Transfer.create(info, origin=wallet, target=self, amount=amount)

    async def unload(self, info: GraphQLResolveInfo, amount: float) -> None:
        """Unoad funds into the wallet the card is attached to."""
        if self.userId != get_user_id(info):
            raise ValueError("You are not allowed to unload this card.")

        wallet = await Wallet.get(info, self.walletId)
        await Transfer.create(info, origin=self, target=wallet, amount=amount)

    async def _update_status(
        self, info: GraphQLResolveInfo, blocked: bool
    ) -> None:
        if self.id:
            result = await get_db(info).cards.update_one(
                {"_id": bson.ObjectId(self.id)}, {"$set": {"blocked": blocked}}
            )
            if result.modified_count == 0:
                raise ValueError(
                    "Cannot update Card with ID:{}".format(self.id)
                )

        self.blocked = blocked

    async def block(self, info: GraphQLResolveInfo) -> None:
        """Block the card and unload all its funds."""
        if self.userId != get_user_id(info):
            raise ValueError("You are not allowed to block this card.")

        await self._update_status(info, blocked=True)
        if await self.balance(info) > 0:
            await self.unload(info, amount=await self.balance(info))

    async def unblock(self, info: GraphQLResolveInfo) -> None:
        """Unblock the card."""
        if self.userId != get_user_id(info):
            raise ValueError("You are not allowed to unblock this card.")

        await self._update_status(info, blocked=False)
