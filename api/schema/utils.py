"""GraphQL resolver utility functions module."""
from graphql import GraphQLResolveInfo
from motor.motor_asyncio import AsyncIOMotorDatabase

from api.rates import RATES, ExchangeRatesAPIClient
from api.mongodb import MONGODB
from api.schema.currency import Currency


def get_db(info: GraphQLResolveInfo) -> AsyncIOMotorDatabase:
    """Get the database from the resolve info."""
    return info.context.app[MONGODB]


def get_rates(info: GraphQLResolveInfo) -> ExchangeRatesAPIClient:
    """Get the exchange rates API client."""
    return info.context.app[RATES]


def get_company_id(info: GraphQLResolveInfo) -> str:
    """Get the Company-Id HTTP header value from the resolve info."""
    company_id = info.context.headers.get("Company-Id")
    if not company_id:
        raise ValueError(
            "You must provide a 'Company-Id' HTTP header with a value."
        )
    return company_id


def get_user_id(info: GraphQLResolveInfo) -> str:
    """Get the User-Id HTTP header value from the resolve info."""
    user_id = info.context.headers.get("User-Id")
    if not user_id:
        raise ValueError(
            "You must provide a 'User-Id' HTTP header with a value."
        )
    return user_id


def get_master_wallet(info: GraphQLResolveInfo, currency: Currency):
    """Get master wallet for this currency."""
    return info.context.app["MASTER_WALLETS"][currency]
