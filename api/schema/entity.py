"""Entity module."""
from abc import ABC, abstractmethod
from dataclasses import asdict
from typing import Optional, Type, TypeVar

import bson
from graphql import GraphQLResolveInfo

from api.schema.utils import get_db

T = TypeVar("T", bound="Entity")


class Entity(ABC):
    """A storable (persistent) type with a unique ID."""

    id = None

    @property
    @abstractmethod
    def collection(self) -> str:
        """Define a collection name for database storage."""
        raise NotImplementedError

    def __post_init__(self, _id: Optional[str] = None) -> None:
        """Run after dataclass generated '__init__' method."""
        self.id = _id

    @classmethod
    async def get(cls: Type[T], info: GraphQLResolveInfo, id: str) -> T:
        """Get an entity by ID."""
        doc = await get_db(info)[cls.collection].find_one(
            {"_id": bson.ObjectId(id)}
        )
        if not doc:
            raise ValueError("No {} found for ID:{}".format(cls.__name__, id))
        return cls(**doc)

    async def persist(self, info: GraphQLResolveInfo) -> None:
        """Store (persist) an entity."""
        result = await get_db(info)[self.collection].insert_one(asdict(self))
        self.id = result.inserted_id
