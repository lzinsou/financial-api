"""GraphiQL module."""
from api.graphiql.middleware import graphiql

__all__ = ["graphiql"]
