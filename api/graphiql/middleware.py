from aiohttp.web import middleware
from api.graphiql.render import render_graphiql


@middleware
async def graphiql(request, handler):
    if all(
        [
            request.method.lower() == "get",
            "raw" not in request.query,
            any(
                [
                    "text/html" in request.headers.get("accept", {}),
                    "*/*" in request.headers.get("accept", {}),
                ]
            ),
        ]
    ):
        return await render_graphiql(
            query=request.query.get("query"),
            variables=request.query.get("variables"),
            operation_name=request.query.get("operationName"),
        )

    return await handler(request)
