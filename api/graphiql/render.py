import os
from json import dumps
from string import Template

from aiohttp.web import Response

GRAPHIQL_VERSION = "0.13.2"
TEMPLATE_PATH = "template.html"


with open(os.path.join(os.path.dirname(__file__), TEMPLATE_PATH)) as file:
    TEMPLATE = file.read()


async def render_graphiql(query=None, variables=None, operation_name=None):
    tpl = Template(TEMPLATE)
    source = tpl.substitute(
        graphiql_version=GRAPHIQL_VERSION,
        query=dumps(query),
        variables=dumps(variables) or "",
        operation_name=dumps(operation_name) or "null",
    )
    return Response(text=source, content_type="text/html")
