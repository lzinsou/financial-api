"""Exchange rates client resource for an AIOHTTP web app."""
import logging
from typing import AsyncIterator

import aiohttp
from aiohttp import web

logger = logging.getLogger(__name__)

RATES = "rates"
RATES_URL = "https://api.exchangeratesapi.io/latest"


class ExchangeRatesAPIClient:
    """The exchange rates API HTTP client."""

    def __init__(self, session: aiohttp.ClientSession) -> None:
        self.session = session

    async def latest_rate(self, base: str, symbol: str) -> float:
        params = {"base": base.upper(), "symbols": symbol.upper()}
        async with self.session.get(RATES_URL, params=params) as response:
            data = await response.json()

            if not data:
                logger.warning("Exchange rates API error: %s", response.status)
                raise RuntimeError("Failed to convert funds. Aborting.")

            return data["rates"][symbol.upper()]

    async def close(self) -> None:
        await self.session.close()


def register_rates(app: web.Application) -> None:
    """Configure a ExchangeRatesAPIClient and register it to the web app."""

    async def rates(app: web.Application) -> AsyncIterator[None]:
        client = ExchangeRatesAPIClient(session=aiohttp.ClientSession())
        app[RATES] = client
        yield
        await client.close()

    app.cleanup_ctx.append(rates)
