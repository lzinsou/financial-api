"""Master wallet fixture."""
import logging
from dataclasses import asdict

from aiohttp import web
from motor.motor_asyncio import AsyncIOMotorDatabase

from api.mongodb import MONGODB
from api.schema.currency import Currency
from api.schema.wallet import Wallet

MASTER_WALLETS = "MASTER_WALLETS"

logger = logging.getLogger(__name__)


async def setup_master_wallets(app: web.Application) -> None:
    """Get or create existing master wallets & register their IDs to the app"""
    db = app[MONGODB]
    app[MASTER_WALLETS] = {}
    for currency in Currency:
        doc = await db[Wallet.collection].find_one(
            {"isMaster": True, "currency": currency}
        )
        if doc:
            app[MASTER_WALLETS][currency] = Wallet(**doc)
            logger.info(
                "Using existing master wallet for %s: %s", currency, doc["_id"]
            )
        else:
            wallet = Wallet(
                isMaster=True, companyId=app["OWNER"], currency=currency, sequenceNumber=0
            )
            result = await db[Wallet.collection].insert_one(asdict(wallet))
            wallet.id = result.inserted_id
            app[MASTER_WALLETS][currency] = wallet
            logger.info(
                "Creating new master wallet for %s: %s", currency, wallet.id
            )
