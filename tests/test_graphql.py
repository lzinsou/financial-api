"""Test GraphQL over HTTP."""
# pylint: disable=redefined-outer-name,
import pytest
from aiohttp import web
from graphql import (
    GraphQLField,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
)

from dolores.errors import (
    ERROR,
    ArgumentTypeError,
    BadJSONBodyError,
    BadJSONVariablesError,
    MissingQueryError,
    UnsupportedContentType,
    json_errors,
)
from dolores.graphql import (
    DATA,
    DEFAULT_GRAPHQL_PATH as GRAPHQL_PATH,
    QUERY,
    ContentType,
    merge_args,
    parse_body,
    parse_query,
    parse_query_variables,
    register_handler,
    VARIABLES,
    OPERATION_NAME,
    GraphQLArgs,
)

HTTP_OK = 200
SCHEMA = GraphQLSchema(
    query=GraphQLObjectType(
        name="RootQueryType",
        fields={
            "test": GraphQLField(
                GraphQLString, resolve=lambda obj, info: "It works!"
            )
        },
    )
)


@pytest.fixture
def client(loop, aiohttp_client):
    """Provide a test HTTP client."""
    app = web.Application(middlewares=[json_errors])
    register_handler(app, SCHEMA)
    return loop.run_until_complete(aiohttp_client(app))


async def test_parse_body_json():
    """When content type is 'application/json', parse the body as JSON."""

    async def body_text():
        assert False

    async def body_json():
        return {"BODY": "JSON"}

    assert (
        await parse_body(
            body_json=body_json,
            body_text=body_text,
            content_type=ContentType.JSON,
        )
        == await body_json()
    )


async def test_parse_body_graphql():
    """When content type is 'application/graphql', parse the body as text."""

    async def body_text():
        assert "TEXT BODY"

    async def body_json():
        return False

    assert await parse_body(
        body_json=body_json,
        body_text=body_text,
        content_type=ContentType.GRAPHQL,
    ) == {QUERY: await body_text()}


async def test_parse_body_unsupported_content_type():
    """When content type is unsupported, raise an exception."""

    async def body_text():
        assert False

    async def body_json():
        return False

    with pytest.raises(UnsupportedContentType):
        await parse_body(
            body_json=body_json,
            body_text=body_text,
            content_type="bad/content",
        )


@pytest.mark.parametrize(
    "raw_variables,variables", [("{}", {}), ('{"foo": "bar"}', {"foo": "bar"})]
)
def test_parse_query_variables(raw_variables, variables):
    """It parses the variables string into a dict."""
    assert parse_query_variables(raw_variables) == variables


def test_parse_query_variables_bad_json():
    """It raises an error it fails to parse the provided string as JSON."""
    with pytest.raises(BadJSONVariablesError):
        parse_query_variables("{bad content")


def test_parse_query_variables_ignore_non_dict():
    """It ignores values that are not a dict and returns an empty dict."""
    assert parse_query_variables('"text"') == {}


@pytest.mark.parametrize(
    "query,query_args",
    [
        ({}, {}),
        ({"query": "{test}"}, {"query": "{test}"}),
        (
            {"query": "{test}", "variables": None},
            {"query": "{test}", "variables": None},
        ),
        (
            {"query": "{test}", "variables": '{"foo": "bar"}'},
            {"query": "{test}", "variables": {"foo": "bar"}},
        ),
        (
            {"query": "{test}", "variables": '{"foo": "bar"}'},
            {"query": "{test}", "variables": {"foo": "bar"}},
        ),
        (
            {"query": "{test}", "operationName": "op"},
            {"query": "{test}", "operationName": "op"},
        ),
        (
            {
                "query": "{test}",
                "variables": '{"foo": "bar"}',
                "operationName": "op",
            },
            {
                "query": "{test}",
                "variables": {"foo": "bar"},
                "operationName": "op",
            },
        ),
        (
            {
                "query": "{test}",
                "variables": '"ignored string"',
                "operationName": "op",
            },
            {"query": "{test}", "variables": {}, "operationName": "op"},
        ),
        (
            {
                "query": "{test}",
                "variables": '"ignored string"',
                "operationName": None,
            },
            {"query": "{test}", "variables": {}, "operationName": None},
        ),
    ],
)
def test_parse_query(query, query_args):
    """It parses the HTTP request query into a dict of arguments."""
    assert parse_query(query) == query_args


@pytest.mark.parametrize(
    "query_args,body_args,merged_args",
    [
        ({"query": "{test}"}, None, {"query": "{test}"}),
        ({"query": "{test}"}, {}, {"query": "{test}"}),
        ({}, {"query": "{test}"}, {"query": "{test}"}),
        ({"query": "{test}"}, {"query": "{ignored}"}, {"query": "{test}"}),
        (
            {"query": "{test}", "variables": {"foo": "bar"}},
            {"query": "{test}"},
            {"query": "{test}", "variables": {"foo": "bar"}},
        ),
        (
            {"query": "{test}"},
            {"query": "{test}", "variables": {"foo": "bar"}},
            {"query": "{test}", "variables": {"foo": "bar"}},
        ),
        (
            {"query": "{test}", "variables": None},
            {"query": "{test}", "variables": {"foo": "bar"}},
            {"query": "{test}", "variables": None},
        ),
        (
            {"query": "{test}", "variables": {}},
            {"query": "{test}", "variables": {"foo": "bar"}},
            {"query": "{test}", "variables": {}},
        ),
        (
            {"query": "{test}", "operationName": "test"},
            {"query": "{test}"},
            {"query": "{test}", "operationName": "test"},
        ),
        (
            {"query": "{test}"},
            {"query": "{test}", "operationName": "test"},
            {"query": "{test}", "operationName": "test"},
        ),
        (
            {"query": "{test}", "operationName": "test"},
            {"query": "{test}", "operationName": "ignored"},
            {"query": "{test}", "operationName": "test"},
        ),
        (
            {"query": "{test}", "operationName": None},
            {"query": "{test}", "operationName": "ignored"},
            {"query": "{test}", "operationName": None},
        ),
        (
            {"query": "{test}", "operationName": ""},
            {"query": "{test}", "operationName": "ignored"},
            {"query": "{test}", "operationName": ""},
        ),
    ],
)
def test_merge_args(query_args, body_args, merged_args):
    """It merges query args and body args into GraphQL args.

    Query args take precedence.
    """
    assert merge_args(query_args, body_args) == GraphQLArgs(
        query=merged_args[QUERY],
        variables=merged_args.get(VARIABLES) or {},
        operation_name=merged_args.get(OPERATION_NAME) or "",
    )


def test_merge_args_missing_query():
    """It raises an error the GraphQL query is missin after merge."""
    with pytest.raises(MissingQueryError):
        merge_args({})


async def test_get_missing_query(client):
    """Query is missing: it responds with an HTTP 400 error."""
    resp = await client.get(GRAPHQL_PATH)
    assert resp.status == MissingQueryError.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert data[ERROR] == MissingQueryError.reason


async def test_get_bad_variables(client):
    """Variables are not valid JSON: it responds with an HTTP 400 error."""
    resp = await client.get(
        GRAPHQL_PATH, params={"query": "{test}", "variables": "{bad json"}
    )
    assert resp.status == BadJSONVariablesError.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert BadJSONVariablesError.reason in data[ERROR]


@pytest.mark.parametrize(
    "query_params",
    [
        ({"query": "{test}"}),
        ({"query": "{test}", "variables": '{"foo": "bar"}'}),
        ({"query": "query Test {test}", "operationName": "Test"}),
        (
            {
                "query": "query Test {test}",
                "variables": '{"foo": "bar"}',
                "operationName": "Test",
            }
        ),
    ],
)
async def test_get_ok(client, query_params):
    """Required arguments are present: it responds with an HTTP 200 OK."""
    resp = await client.get(GRAPHQL_PATH, params=query_params)
    assert resp.status == HTTP_OK
    data = await resp.json()
    assert isinstance(data, dict)
    assert DATA in data


@pytest.mark.parametrize(
    "query,response_fields",
    [("{test}", {"data"}), ("{bad}", {"data", "errors"})],
)
async def test_get_graphql_response(client, query, response_fields):
    """'errors' field is only present in the response if execution fails."""
    resp = await client.get(GRAPHQL_PATH, params={"query": query})
    assert resp.status == HTTP_OK
    data = await resp.json()
    assert isinstance(data, dict)
    for field in response_fields:
        assert field in data


@pytest.mark.parametrize(
    "content_type,body",
    [
        (ContentType.JSON, {"json": None}),
        (ContentType.GRAPHQL, {"data": None}),
    ],
)
async def test_post_missing_query(client, content_type, body):
    """Query is missing: it responds with an HTTP 400 error."""
    resp = await client.post(
        GRAPHQL_PATH, headers={"Content-Type": format(content_type)}, **body
    )
    assert resp.status == MissingQueryError.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert data[ERROR] == MissingQueryError.reason


async def test_post_bad_variables(client):
    """Variables are not valid JSON: it responds with an HTTP 400 error."""
    resp = await client.post(
        GRAPHQL_PATH, params={"query": "{test}", "variables": "{bad json"}
    )
    assert resp.status == BadJSONVariablesError.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert BadJSONVariablesError.reason in data[ERROR]


async def test_post_bad_body(client):
    """Variables are not valid JSON: it responds with an HTTP 400 error."""
    resp = await client.post(
        GRAPHQL_PATH,
        headers={"Content-Type": format(ContentType.JSON)},
        data="{bad json",
    )
    assert resp.status == BadJSONBodyError.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert BadJSONBodyError.reason in data[ERROR]


async def test_post_unsupported_content_type(client):
    """Content type is not supported: it responds with an HTTP 415 error."""
    resp = await client.post(
        GRAPHQL_PATH, headers={"Content-Type": "foo/bar"}, data="baz"
    )
    assert resp.status == UnsupportedContentType.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert data[ERROR] == UnsupportedContentType(supported=ContentType).reason


async def test_post_argument_type_error(client):
    """Variables are not a dict: it responds with an HTTP 400 error."""
    resp = await client.post(
        GRAPHQL_PATH, json={"query": "{test}", "variables": "string"}
    )
    assert resp.status == ArgumentTypeError.status_code
    data = await resp.json()
    assert isinstance(data, dict)
    assert ERROR in data
    assert ArgumentTypeError.reason in data[ERROR]


@pytest.mark.parametrize(
    "content_type,params,body",
    [
        (ContentType.JSON, None, {"json": {"query": "{test}"}}),
        (ContentType.GRAPHQL, None, {"data": "{test}"}),
        (ContentType.JSON, {"query": "{test}"}, {"json": None}),
        (ContentType.GRAPHQL, {"query": "{test}"}, {"data": None}),
    ],
)
async def test_post_ok(client, content_type, params, body):
    """Required arguments are present: it responds with an HTTP 200 OK."""
    resp = await client.post(
        GRAPHQL_PATH,
        headers={"Content-Type": format(content_type)},
        params=params,
        **body,
    )
    assert resp.status == HTTP_OK
    data = await resp.json()
    assert isinstance(data, dict)
    assert DATA in data


@pytest.mark.parametrize(
    "query,response_fields",
    [("{test}", {"data"}), ("{bad}", {"data", "errors"})],
)
async def test_post_graphql_response(client, query, response_fields):
    """'errors' field is only present in the response if execution fails."""
    resp = await client.post(GRAPHQL_PATH, json={"query": query})
    assert resp.status == 200
    data = await resp.json()
    assert isinstance(data, dict)
    for field in response_fields:
        assert field in data
